output "lb_target_group_arns" {
  value = try(aws_lb_target_group.main.*.arn, "")
}

output "lb_dns_name" {
  value = try(concat(aws_lb.this.*.dns_name, [""])[0], "")
}

output "lb_zone_id" {
  value = try(concat(aws_lb.this.*.zone_id, [""])[0], "")
}
