variable "name" {
  type = string
}
variable "internal" {
  type    = bool
  default = false
}
variable "create_lb" {
  type    = bool
  default = false
}
variable "ssl_policy" {
  type    = string
  default = "ELBSecurityPolicy-2016-08"
}
variable "load_balancer_type" {
  type    = string
  default = "application"
}

variable "tags" {}
variable "vpc_id" {}
variable "subnets" {}
variable "target_groups" {}
variable "security_groups" {}
variable "certificate_arn" {}
