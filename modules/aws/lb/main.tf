resource "aws_lb" "this" {
  count              = var.create_lb ? 1 : 0
  name               = var.name
  internal           = var.internal
  load_balancer_type = var.load_balancer_type
  subnets            = var.subnets
  security_groups    = var.security_groups

  enable_deletion_protection = false

  tags = var.tags
}

resource "aws_lb_target_group" "main" {
  count = var.create_lb ? length(var.target_groups) : 0

  name        = lookup(var.target_groups[count.index], "name", null)
  name_prefix = lookup(var.target_groups[count.index], "name_prefix", null)

  vpc_id           = var.vpc_id
  port             = lookup(var.target_groups[count.index], "backend_port", null)
  protocol         = lookup(var.target_groups[count.index], "backend_protocol", null) != null ? upper(lookup(var.target_groups[count.index], "backend_protocol")) : null
  protocol_version = lookup(var.target_groups[count.index], "protocol_version", null) != null ? upper(lookup(var.target_groups[count.index], "protocol_version")) : null
  target_type      = lookup(var.target_groups[count.index], "target_type", null)

  dynamic "health_check" {
    for_each = length(keys(lookup(var.target_groups[count.index], "health_check", {}))) == 0 ? [] : [lookup(var.target_groups[count.index], "health_check", {})]

    content {
      enabled             = lookup(health_check.value, "enabled", null)
      interval            = lookup(health_check.value, "interval", null)
      path                = lookup(health_check.value, "path", null)
      port                = lookup(health_check.value, "port", null)
      healthy_threshold   = lookup(health_check.value, "healthy_threshold", null)
      unhealthy_threshold = lookup(health_check.value, "unhealthy_threshold", null)
      timeout             = lookup(health_check.value, "timeout", null)
      protocol            = lookup(health_check.value, "protocol", null)
      matcher             = lookup(health_check.value, "matcher", null)
    }
  }
}

resource "aws_lb_target_group_attachment" "main" {
  count            = var.create_lb ? length(var.target_groups) : 0
  target_group_arn = aws_lb_target_group.main[count.index].arn
  target_id        = lookup(var.target_groups[count.index], "target_id", null)
  port             = lookup(var.target_groups[count.index], "backend_port", null)
}

resource "aws_lb_listener" "https" {
  count = (length(var.target_groups) != 0 && var.create_lb) ? 1 : 0

  load_balancer_arn = aws_lb.this[0].arn

  port            = "443"
  protocol        = "HTTPS"
  ssl_policy      = var.ssl_policy
  certificate_arn = var.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.main[0].arn
  }
}

resource "aws_lb_listener_rule" "https" {
  count        = var.create_lb ? length(var.target_groups) : 0
  listener_arn = aws_lb_listener.https[0].arn
  priority     = 1 + count.index

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.main[count.index].arn
  }

  condition {
    host_header {
      values = lookup(var.target_groups[count.index], "host_header", null)
    }
  }
}

resource "aws_lb_listener" "http_to_https" {
  load_balancer_arn = aws_lb.this[0].arn

  port     = "80"
  protocol = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
