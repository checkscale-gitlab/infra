[![pipeline status](https://gitlab.com/staboss/infra/badges/develop/pipeline.svg)](https://gitlab.com/staboss/infra/-/commits/develop)

# Infra

Launch services in `AWS`

- Create infra with `Terraform`
- Config hosts with `Ansible`

### List of services

- [x] `Gogol`
- [x] `Runner`
- [x] `Prometheus`
- [x] `Grafana`

### Infrastructure resources

- `ACM`
- `ALB`
- `EC2`
- `VPC`
- `Route53`

# Terraform

### Init

```
terraform init -backend-config=config/$ENV/$ENV-backend.conf -backend-config="password=$GITLAB_TOKEN"
terraform fmt
terraform validate
```

### Plan

```
terraform plan -var-file=config/$ENV/$ENV-.tfvars --out plan-$ENV
```

### Apply

```
terraform apply --auto-approve plan-$ENV
```

# Ansible

### Generate inventory

```
chmod +x inv.sh
./inv.sh
```

### Base Config

```
make base
```

### Runner Config

```
make runner

ssh $REMOTE_USER@$REMOTE_HOST -i $PRIVATE_KEY_FILE

sudo docker exec -it $RUNNER_CONTAINER /bin/bash
/# apt-get update
/# apt-get install nano
/# nano /etc/gitlab-runner/config.toml
   -> set privileged = true

sudo docker restart $RUNNER_CONTAINER
```

### Monitoring Config

```
make monitoring
```

### Node Exporter Config

```
make node_exporter
```

Use the `ansible_vault_pass` provided to you via `1Password` or any other password manager

# Grafana

```
Go Service Dashboard ID = 14061
Node State Dashboard ID = 1860
```
