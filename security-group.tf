module "web_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/web"
  version = "~> 4.0"

  name   = "web-sg-${var.environment}"
  vpc_id = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
}

module "ssh_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/ssh"
  version = "~> 4.0"

  name   = "ssh-sg-${var.environment}"
  vpc_id = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
}

module "grafana_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/grafana"
  version = "~> 4.0"

  name   = "grafana-sg-${var.environment}"
  vpc_id = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
}

module "prometheus_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/prometheus"
  version = "~> 4.0"

  name   = "prometheus-sg-${var.environment}"
  vpc_id = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
}

module "node_exporter_security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name   = "node-exporter-sg-${var.environment}"
  vpc_id = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 9100
      to_port     = 9100
      protocol    = "tcp"
      description = "Node exporter service port"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}
