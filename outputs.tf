### ALB ###
output "alb_dns_name" {
  value = module.alb.lb_dns_name
}

### IPs ###
output "gogol_public_ip" {
  value = module.gogol.public_ip
}

output "runner_public_ip" {
  value = module.runner.public_ip
}

output "monitoring_public_ip" {
  value = module.monitoring.public_ip
}
