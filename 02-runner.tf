module "runner" {
  source = "./modules/aws/ec2"

  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  subnet_id     = module.vpc.public_subnets[1]
  key_name      = var.key_name
  eip_enabled   = false

  vpc_security_group_ids = [
    module.ssh_security_group.security_group_id
  ]

  tags = {
    Name        = "runner-${var.environment}"
    Terraform   = "true"
    Environment = var.environment
  }
}
