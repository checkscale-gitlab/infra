variable "domain" {
  type = string
}

variable "subdomain" {
  type    = string
  default = "test."
}

variable "grafana_subdomain" {
  type    = string
  default = "grafana-test."
}

variable "monitoring_subdomain" {
  type    = string
  default = "monitoring-test."
}

variable "server_node_subdomain" {
  type    = string
  default = "server-node-test."
}

variable "key_name" {
  type = string
}

variable "environment" {
  type    = string
  default = "test"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "tags" {
}

variable "region" {
  type    = string
  default = "us-east-1"
}

variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "public_subnet_count" {
  type    = number
  default = 2
}

variable "private_subnet_count" {
  type    = number
  default = 2
}

variable "public_subnet_cidr_blocks" {
  type = list(string)
  default = [
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24",
    "10.0.4.0/24",
    "10.0.5.0/24",
    "10.0.6.0/24",
    "10.0.7.0/24",
    "10.0.8.0/24",
  ]
}

variable "private_subnet_cidr_blocks" {
  type = list(string)
  default = [
    "10.0.101.0/24",
    "10.0.102.0/24",
    "10.0.103.0/24",
    "10.0.104.0/24",
    "10.0.105.0/24",
    "10.0.106.0/24",
    "10.0.107.0/24",
    "10.0.108.0/24",
  ]
}

variable "create_lb" {
  type    = bool
  default = true
}

variable "zone_id" {
  type = string
}
