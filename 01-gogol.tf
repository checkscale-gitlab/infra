module "gogol" {
  source = "./modules/aws/ec2"

  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  subnet_id     = module.vpc.public_subnets[0]
  key_name      = var.key_name
  eip_enabled   = true

  vpc_security_group_ids = [
    module.web_security_group.security_group_id,
    module.ssh_security_group.security_group_id,
    module.node_exporter_security_group.security_group_id
  ]

  tags = {
    Name        = "gogol-${var.environment}"
    Terraform   = "true"
    Environment = var.environment
  }
}
