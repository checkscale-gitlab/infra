module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 3.0"

  zone_id = var.zone_id

  domain_name               = var.domain
  subject_alternative_names = ["*.${var.domain}"]

  wait_for_validation = true
}
